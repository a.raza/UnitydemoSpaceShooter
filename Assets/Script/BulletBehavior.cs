﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour 
{
    public float m_speed;
    internal float m_lifeOfBullet;
    private Rigidbody m_rigidbody;
    private float m_currentTime { get; set; }

    // Use this for initialization
    private void Awake () 
    {
        GetRigidbody();
	}

	private void OnEnable()
	{
        m_currentTime = 0;
        m_rigidbody.velocity = -transform.forward * m_speed;
	}

	// Update is called once per frame
    private void Update () 
    {
        MoveBullet();
        DisableBulletAfteSomeTime();
    }

    private void GetRigidbody()
    {
        if (this.GetComponent<Rigidbody>().Equals(null))
        {
            m_rigidbody = this.gameObject.AddComponent<Rigidbody>();
        }
        else
        {
            m_rigidbody = this.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void MoveBullet()
    {
        m_rigidbody.AddForce(-transform.forward * m_speed);
    }

	private void DisableBulletAfteSomeTime()
	{
        m_currentTime += Time.fixedDeltaTime;

        if (m_currentTime > m_lifeOfBullet)
        {
            this.gameObject.SetActive(false);

            UIManager.instance.GetAndSetAvailableBulletImage(true);
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        this.gameObject.SetActive(false);
    }
}
