﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelDifficulty
{
    Easy,
    Normal,
    Hard,
}

[System.Serializable]
public class Wave
{
    public List<List<GameObject>> 
    m_meteors = new List<List<GameObject>>() ,
    m_enemyShips = new List<List<GameObject>>();
}

public class WaveOperator : MonoBehaviour 
{
    public static WaveOperator instance = null;

    public LevelDifficulty m_levelDifficulty;

    public GameObject[] m_enemyShipsPrefab;
    public GameObject[] m_meteorsPrefab;

    private Transform[] m_spawnPoints;

    public Wave m_wave = new Wave();

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

	private void Start()
	{
        GetSpawnPoints();
        StartCoroutine(SetWaveSpawningLocation());

        switch(m_levelDifficulty)
        {
            case LevelDifficulty.Normal:
                CreateObstacles(20, 20);
                // 2 a 2 e
                // 3 a 3 e
                // 4 a 4 e
                // 5 a 5 e
                // 6 a 6 e
                break;

            case LevelDifficulty.Hard:
                CreateObstacles(25, 25);
                // 3 a 3 e
                // 4 a 4 e
                // 5 a 5 e
                // 6 a 6 e
                // 7 a 7 e
                break;

            default:
                CreateObstacles(10, 10);
                StartCoroutine(ManageWave());
                // 2 a 1 e
                // 2 a 2 e
                // 3 a 3 e
                // 3 a 4 e
                break;
        }
	}

    IEnumerator ManageWave()
    {
        for (int i = 0; i < m_wave.m_meteors.Count; i++)
        {
            yield return new WaitForSeconds(1f);

            var rand = Random.Range(0, m_spawnPoints.Length);

            m_spawnPoints[rand].position = new Vector3(m_spawnPoints[rand].position.x, m_spawnPoints[rand].position.y, 10);

            var a_object = ObjectPooling.instance.GetAvailableObject(m_wave.m_meteors[i], m_spawnPoints[rand], Quaternion.identity, true);

            if (!a_object.Equals(null))
                break;
        }
    }

    private void GetSpawnPoints()
    {
        m_spawnPoints = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            m_spawnPoints[i] = transform.GetChild(i);
        }
    }

    IEnumerator SetWaveSpawningLocation()
    {
        yield return new WaitForSeconds(0.5f);

        var a_yMax = PlayerControlls.instance.m_boundary.yMax;

        transform.position = new Vector3(0, a_yMax + 5, 10);

        var a_xMin = PlayerControlls.instance.m_boundary.xMin;
        var a_xMax = PlayerControlls.instance.m_boundary.xMax;

        var a_pointsBetween = a_xMin - a_xMax;

        for (int i = 0; i < m_spawnPoints.Length; i++)
        {
            m_spawnPoints[i].localPosition = new Vector3
                (
                    (a_xMax * i) + a_pointsBetween+a_xMin,
                    a_yMax + 5,
                    10
                );
        }
    }

    private void CreateObstacles(int a_amountOfMeteors, int amountOfEnemyShips)
    {
        for (int i = 0; i < m_meteorsPrefab.Length; i++)
        {
            var a_meteors = new List<GameObject>();

            a_meteors = ObjectPooling.instance.CreatePool(m_meteorsPrefab[i], ((int)a_amountOfMeteors/m_meteorsPrefab.Length), false);

            m_wave.m_meteors.Add(a_meteors);
        }

        //for (int i = 0; i < m_enemyShipsPrefab.Length; i++)
        //{
        //    m_wave.m_enemyShips = ObjectPooling.instance.CreatePool(m_enemyShipsPrefab[i], ((int)amountOfEnemyShips / m_enemyShipsPrefab.Length), false);
        //}
    }
}
