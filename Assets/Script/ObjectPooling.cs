﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    public static ObjectPooling instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public List<GameObject> CreatePool(GameObject a_prefab, int a_amountOfObjects, bool a_setActive)
    {
        if (a_prefab == null)
            return null;
        
        List<GameObject> a_objects = new List<GameObject>();

        for (int i = 0; i < a_amountOfObjects; i++)
        {
            var a_object = Instantiate(a_prefab, a_prefab.transform.position, Quaternion.identity) as GameObject;

            a_object.SetActive(a_setActive);

            a_objects.Add(a_object);
        }

        return a_objects;
    }

    /// <summary>
    /// Gets The Available Object In the Pool.
    /// </summary>
    /// <param name="a_objects"> List Of GameObjects.</param>
    /// <param name="a_transform">A transform to manage the position of the object.</param>
    /// <param name="a_quaternion">A quaternion.</param>
    /// <param name="a_setActive">Should the object become inactive or active.</param>
    public GameObject GetAvailableObject(List<GameObject> a_objects, Transform a_transform, Quaternion a_quaternion, bool a_setActive)
    {
        //if the pool is not availale
        if (a_objects.Equals (null))
            return null; //return

        foreach(GameObject a_object in a_objects)
        {
            if(a_object.activeInHierarchy.Equals(false))
            {
                if (!a_transform.Equals (null))
                    a_object.transform.position = a_transform.position;

                if (!a_quaternion.Equals(null))
                    a_object.transform.rotation = a_quaternion;

                a_object.SetActive(a_setActive);

                return a_object;
            }
        }

        return null;
    }
}