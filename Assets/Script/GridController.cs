﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridController : MonoBehaviour 
{
    private int m_col;
    public RectTransform m_parent;

    [Range(40,50)]
    public float m_imageSize;
    private GridLayoutGroup m_grid;

    // Use this for initialization
    void Start()
    {
        GetGrid();

        UpdateCells();
    }

    void Update()
    {
        UpdateCells(); //this is only necessery when the developer is going to change the resolution simultaneously
    }

    //following method updates the cells in the m_grid and arrange the UI bullet images
    public void UpdateCells()
    {
        if (m_parent.Equals(null))
            return;
        
        ////getting total number of bullets, and adding one to them, to make sure the images are aligned appropriately
        m_col = PlayerControlls.instance.m_amountOfSingleBullets + 1;

        var a_height = (m_parent.rect.height / 100) * m_imageSize;
        var a_width = (m_parent.rect.width / 100) * m_imageSize;

        ////getting the m_grid, and changing the height and width of each cell in the m_grid
        m_grid.cellSize = new Vector2(a_height / m_col, a_width / m_col); // the height and width should have the same value, making a proper square, allowing any image to blend in the UI
    }

    private void GetGrid()
    {
        if (GetComponent<GridLayoutGroup>().Equals(null))
            m_grid = this.gameObject.AddComponent<GridLayoutGroup>();
        else
            m_grid = GetComponent<GridLayoutGroup>();
    }
}
