﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehavior : MonoBehaviour 
{
    public float m_speed;
    public float m_lifeOfAsteroid;
    private Rigidbody m_rigidbody;
    private float m_currentTime { get; set; }
    public Collider m_collider;
    private MeshRenderer m_mesh;
    private bool m_fade = false;
    public GameObject m_vfx;

    // Use this for initialization
    private void Awake()
    {
        m_mesh = GetComponent<MeshRenderer>();
        GetRigidbody();
    }

    private void OnEnable()
    {
        m_currentTime = 0;
        m_rigidbody.velocity = new Vector3(0, -m_speed, 0);
    }

    private void Update()
    {
        MoveBullet();
        DisableBulletAfteSomeTime();

        FadeAway();
    }

    private void MoveBullet()
    {
        m_rigidbody.AddForce(new Vector3(0, -m_speed, 0));
        m_rigidbody.AddForceAtPosition(Vector3.forward, new Vector3(0, -m_speed, 0));
    }

    private void DisableBulletAfteSomeTime()
    {
        m_currentTime += Time.fixedDeltaTime;

        if (m_currentTime > m_lifeOfAsteroid)
        {
            this.gameObject.SetActive(false);

            UIManager.instance.GetAndSetAvailableBulletImage(true);
        }
    }

    private void GetRigidbody()
    {
        if (this.GetComponent<Rigidbody>().Equals(null))
        {
            m_rigidbody = this.gameObject.AddComponent<Rigidbody>();
        }
        else
        {
            m_rigidbody = this.gameObject.GetComponent<Rigidbody>();
        }
    }

	private void OnCollisionEnter(Collision collision)
	{
        if(collision.gameObject.tag.Equals("PBlaster"))
        {
            m_collider.enabled = false;

            m_fade = true;
        }
	}

    float a_aplha = 0f;

    private void FadeAway()
    {
        if(m_fade.Equals(true))
        {
            m_mesh.material.SetFloat("_Cutoff", a_aplha);

            a_aplha += Time.deltaTime;

            m_mesh.material.SetColor("Color", Color.Lerp(m_mesh.material.color, Color.yellow, a_aplha * 2 ));

            m_mesh.material.SetColor("_EmissionColor", Color.Lerp(m_mesh.material.color, Color.yellow, a_aplha * 2));

            if(!m_vfx.Equals(null))
                m_vfx.SetActive(true);
        }
    }
}
