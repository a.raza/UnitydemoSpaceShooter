﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
    public static UIManager instance = null;
    
    public Image m_bulletImage;
    public Transform m_bulletHub;
    public List<GameObject> m_bulletImages = new List<GameObject>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

	void Start () 
    {
        CreateBulletImages();
	}

    private void CreateBulletImages()
    {
        m_bulletImages = ObjectPooling.instance.CreatePool(m_bulletImage.gameObject, PlayerControlls.instance.m_amountOfSingleBullets, true);

        foreach(GameObject a_image in m_bulletImages)
        {
            a_image.transform.SetParent(m_bulletHub);
            var rect = a_image.GetComponent<RectTransform>();
            rect.transform.localScale = new Vector3(1, 1, 1);
            rect.localPosition = new Vector3(10, -10, 0);
        }
    }

    public void GetAndSetAvailableBulletImage(bool a_value)
    {
        foreach(GameObject a_image in m_bulletImages)
        {
            if(a_value.Equals(true) && a_image.activeInHierarchy.Equals(false))
            {
                a_image.SetActive(a_value);
                break;
            }
            else if (a_value.Equals(false) && a_image.activeInHierarchy.Equals(true))
            {
                a_image.SetActive(a_value);
                break;
            }


        }
    }
}
