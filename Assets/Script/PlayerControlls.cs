﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public enum Projectile
{
    SingleBullet,
    TwinBullet,
    ProBlaster,
}

public class PlayerControlls : MonoBehaviour
{
    public static PlayerControlls instance = null;
    
    public float m_speed;
    public float m_tilt;

    [Range(1.5f, 1.75f)]
    public float m_boundarySize;

    public RectTransform m_rectTransform;
    public GameObject m_shot;

    public int 
    m_amountOfSingleBullets,
    m_amountOfTwinBullets;

    public Transform
    m_shotSpawnForSingleBullet,
    m_shotSpawnPointA,
    m_shotSpawnPointB;
    //public AudioSource m_audio;

    public Projectile m_currentProjectle;
    public float m_bulletRegenerateTime;

    internal Boundary m_boundary = new Boundary();
    private Rigidbody m_rigidbody { get; set; }
    private List<GameObject> 
    m_singleBullets = new List<GameObject>(), 
    m_twinBullets = new List<GameObject>();


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

	private void Start()
	{
        GetRigidbody();

        SetBoundary();

        GenerateBullets();
	}

	private void Update()
    {
        Fire();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 50.0f);
        m_rigidbody.velocity = movement * m_speed;

        m_rigidbody.position = new Vector3
        (
                Mathf.Clamp(m_rigidbody.position.x, m_boundary.xMin, m_boundary.xMax),
                Mathf.Clamp(m_rigidbody.position.y, m_boundary.yMin, m_boundary.yMax),
                0.0f
        );

        m_rigidbody.rotation = Quaternion.Euler(-90.0f, m_rigidbody.velocity.x * -m_tilt, 0.0f);
    }

    private void GetRigidbody()
    {
        if (this.GetComponent<Rigidbody>().Equals(null))
        {
            m_rigidbody = this.gameObject.AddComponent<Rigidbody>();
        }
        else
        {
            m_rigidbody = this.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void SetBoundary()
    {
        var a_rt = m_rectTransform.rect;

        float a_height = (a_rt.height / 100) * m_boundarySize;
        float a_width = (a_rt.width / 100) * m_boundarySize;

        m_boundary.xMax = a_width;
        m_boundary.xMin = -a_width;
        m_boundary.yMax = a_height;
        m_boundary.yMin = -a_height;
    }

    private void GenerateBullets()
    {
        //create bullets
        m_singleBullets = ObjectPooling.instance.CreatePool(m_shot, m_amountOfSingleBullets, false);
        m_twinBullets = ObjectPooling.instance.CreatePool(m_shot, m_amountOfTwinBullets, false);

        BulletRegeneratingManager(m_singleBullets);
        BulletRegeneratingManager(m_twinBullets);
    }

    private void Fire()
    {
        if (Input.GetKeyDown(KeyCode.F) || Input.GetButtonDown("Fire1"))
        {
            var a_bulletRotation = Quaternion.Euler(90, 0, 0);

            switch (m_currentProjectle)
            {
                case Projectile.TwinBullet:
                    ObjectPooling.instance.GetAvailableObject(m_twinBullets, m_shotSpawnPointA, a_bulletRotation, true);
                    ObjectPooling.instance.GetAvailableObject(m_twinBullets, m_shotSpawnPointB, a_bulletRotation, true);
                    break;
                case Projectile.ProBlaster:
                    ObjectPooling.instance.GetAvailableObject(m_singleBullets, m_shotSpawnForSingleBullet, a_bulletRotation, true);
                    ObjectPooling.instance.GetAvailableObject(m_twinBullets, m_shotSpawnPointA, a_bulletRotation, true);
                    ObjectPooling.instance.GetAvailableObject(m_twinBullets, m_shotSpawnPointB, a_bulletRotation, true);
                    break;
                default:
                    ObjectPooling.instance.GetAvailableObject(m_singleBullets, m_shotSpawnForSingleBullet, a_bulletRotation, true);
                    break;
            }

            UIManager.instance.GetAndSetAvailableBulletImage(false);
        }
    }

    private void BulletRegeneratingManager(List<GameObject> a_bulletsHolder)
    {
        foreach(GameObject a_bullet in a_bulletsHolder)
        {
            a_bullet.GetComponent<BulletBehavior>().m_lifeOfBullet = m_bulletRegenerateTime;
        }
    }

	void OnCollisionEnter(Collision collision)
	{
        if(collision.gameObject.tag.Equals("Enemy"))
       {
           Debug.Log("Damage");
       }
	}
}